# FullCalendar demo with React
---
El demo esta contruido utlizando bower como manejador de paquetes, para poder ejecutar el demo:
1. Instalar bower([Bower](https://bower.io/ "Instalar Bower"))
2. Ejecutar los siguientes comandos:
```
cd /path/to/application/
bower install
```
*El demo solo puede ser ejecutado bajo un protocolo http, abrir el archivo index.html en el navegador, no funcionara.*
