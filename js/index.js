var App = React.createClass({
  render: function(){
    return (
      <div>
        <FullCalendar />
      </div>
    );
  }
});

ReactDOM.render(
  <App />,
  document.getElementById('app')
);
