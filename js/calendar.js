window.FullCalendar = React.createClass({
  getInitialState: function(){
    var comp = this;
    return {
      calendarConfig: {
        header: {
          left: 'title',
          center: 'month,agendaDay'
        },
        dayClick: function(obj) {
          $(comp.refs.calendar).fullCalendar('gotoDate', moment(obj));
          $(comp.refs.calendar).fullCalendar('changeView', 'agendaDay')
        },
        eventClick: function(element,e) {

        },
        eventDrop: function(event, delta, revertFunc) {

          alert(event.title + " -> " + event.start.format());

          if (!confirm("Seguro?")) {
            revertFunc();
          }

        },
        fixedWeekCount: false,
        nowIndicator: true,
        selectable: true,
        editable: true,
        eventSources: this.prepareSources()
      }
    }
  },
  prepareSources: function(){
    var dateUtils = DateUtils.new();
    var sources = [];


    var moments = dateUtils.getRandomDatesAsc(50,moment("2016-04-01"));
    var eventos = [];
    for (var i= 0; i < moments.length; i++){
      var id = (i + 1);
      moments[i].add(Utils.getRandomInt(8,20),"h");
      var titulo = (Utils.getRandomInt(1,4)==3)?"Enrique Hernandez":"(1ra) Enrique Hernandez";
      eventos.push({
        id: "c"+id,
        title: titulo,
        start: moments[i],
        end: moments[i].clone().add(Utils.getRandomInt(2,5),"h"),
      });
    }
    sources.push({
      events: eventos,
      color: 'rgba(0,174,239,0.7)',
      editable: true,
      startEditable: true,
      durationEditable: true
    });

    var moments = dateUtils.getRandomDatesAsc(50,moment("2016-05-01"));
    var eventos = [];
    for (var i= 0; i < moments.length; i++){
      var id = (i + 1);
      moments[i].add(Utils.getRandomInt(8,20),"h");
      eventos.push({
        id: "e"+id,
        title: "Evento",
        start: moments[i],
        end: moments[i].clone().add(Utils.getRandomInt(2,5),"h"),
      });
    }
    sources.push({
      events: eventos,
      color: 'rgba(238,177,17,0.7)',
      editable: true,
      startEditable: true,
      durationEditable: true
    });
    return sources;
  },
  componentDidMount: function() {
    $(this.refs.calendar).fullCalendar(this.state.calendarConfig);
  },
  render: function(){
    return (
      <div>
        <div ref="calendar" id="calendar"></div>
      </div>
    );
  }

});
