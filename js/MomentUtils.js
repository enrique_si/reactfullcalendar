var DateUtils = {
  new: function() {
    utils = {
      getActualDate: function() {
        return moment();
      },
      getRandomDatesAsc: function(quantity, startMoment) {
        var moments = [];
        while (quantity > 0) {
          startMoment.add(Utils.getRandomInt(2,6),'day');
          moments.push(startMoment.clone());
          quantity--;
        }
        return moments;
      }
    }
    return utils;
  }
}

var Utils = {
  getRandomInt: function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }
}
